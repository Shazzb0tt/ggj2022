// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function has_player_lost(testedPlayer) {
	with (ObjCastle) {
		if (player == testedPlayer) {
			return false;
		}
	}
	
	return true;
}