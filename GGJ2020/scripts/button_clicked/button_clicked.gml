function button_clicked(button) {
	return button_clicked_position(button.x, button.y, button.x + button.sprite_width, button.y + button.sprite_height);
}

function button_clicked_position(x1, y1, x2, y2) {
	if (mouse_check_button_released(mb_left)) {
		return mouse_in_button(x1, y1, x2, y2);
	}
	return false;
}

function mouse_in_button(x1, y1, x2, y2) {
	var click_x = device_mouse_x_to_gui(0); 
	var click_y = device_mouse_y_to_gui(0); 
	if (point_in_rectangle(click_x, click_y, x1, y1, x2, y2)) {
		return true;
	}
	return false;
}
