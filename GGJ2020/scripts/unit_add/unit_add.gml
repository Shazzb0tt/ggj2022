function unit_add(hex, player, unit_object) {
	var position = get_hex_unit_offset(hex);
	var unit = instance_create_depth(position.x, position.y, 0, unit_object);
	unit.player = player;
	unit.hex = hex;
	//unit.turn_movement_count = 0;
	unit.turn_movement_count = unit.stats.movement_count;
	unit.current_hp = unit.stats.hp;
	ObjMatch.player.selected = [unit];
	return unit;
}

function unit_add_castle(x_hex, y_hex, player) {
	var castle = instance_create_depth(get_x_pixels(x_hex), get_y_pixels(x_hex, y_hex), 0, ObjCastle);
	castle.depth = get_unit_depth(castle);
	castle.player = player;
	castle.hex = ObjMap.tiles[x_hex, y_hex];
	if (player.team == Team.LIGHT) {
		castle.image_index = 0;
	} else if (player.team == Team.DARK) {
		castle.image_index = 1;
	}
	castle.neutral = false;
	castle.current_hp = castle.stats.hp;
	clear_decals(castle.hex);
}

function unit_add_neutral_castle(x_hex, y_hex) {
	var castle = instance_create_depth(get_x_pixels(x_hex), get_y_pixels(x_hex, y_hex), 0, ObjCastle);
	castle.depth = get_unit_depth(castle);
	castle.hex = ObjMap.tiles[x_hex, y_hex];
	castle.image_index = 2;
	castle.current_hp = castle.stats.hp;
	clear_decals(castle.hex);
}

function unit_add_neutral_town(x_hex, y_hex) {
	var town = instance_create_depth(get_x_pixels(x_hex), get_y_pixels(x_hex, y_hex), 0, ObjTown);
	town.depth = get_unit_depth(town);
	town.hex = ObjMap.tiles[x_hex, y_hex];
	if (town.hex.hex_type == HexType.DESERT || town.hex.hex_type == HexType.DESERT2) {
		town.image_index = 3;
	} else {
		town.image_index = 2;
	}
	
	town.current_hp = town.stats.hp;
	clear_decals(town.hex);
}

function unit_add_town(x_hex, y_hex, player) {
	var town = instance_create_depth(get_x_pixels(x_hex), get_y_pixels(x_hex, y_hex), 0, ObjTown);
	town.depth = get_unit_depth(town);
	town.player = player;
	town.hex = ObjMap.tiles[x_hex, y_hex];
	if (player.team == Team.LIGHT) {
		if (town.hex.hex_type == HexType.DESERT || town.hex.hex_type == HexType.DESERT2) {
			town.image_index = 4;
		} else {
			town.image_index = 0;
		}
	} else {
		if (town.hex.hex_type == HexType.DESERT || town.hex.hex_type == HexType.DESERT2) {
			town.image_index = 5;
		} else {
			town.image_index = 1;
		}
	}
	town.neutral = false;
	town.current_hp = town.stats.hp;
	clear_decals(town.hex);
}

function clear_decals(unit_hex) {
	with (ObjGrass) {
		if (hex == unit_hex) {
			instance_destroy();
		}
	}
	with (ObjTree) {
		if (hex == unit_hex) {
			instance_destroy();
		}
	}
	with (ObjTree2) {
		if (hex == unit_hex) {
			instance_destroy();
		}
	}
	with (ObjBush) {
		if (hex == unit_hex) {
			instance_destroy();
		}
	}
}