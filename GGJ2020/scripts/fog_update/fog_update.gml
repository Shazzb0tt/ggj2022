function fog_update() {
	if (global.debug) {
		with (ObjFog) {
			visible = false;
		}
		return;
	}
	
	with (ObjCastle) {
		if (neutral || player != ObjMatch.player) {
			continue;
		}
		fog_show_neighbors(hex, 2);
	}
	
	with (ObjTown) {
		if (neutral || player != ObjMatch.player) {
			continue;
		}
		fog_show_neighbors(hex, 1);
	}
	
	with (ObjUnit) {
		if (player != ObjMatch.player) {
			continue;
		}
		var distance = 1;
		if (hex.hex_type == HexType.MOUNTAIN || hex.hex_type == HexType.DESERT_MOUNTAIN) {
			distance++;
		}
		
		fog_show_neighbors(hex, distance);
		if (array_length(move_to) > 0 && move_to[0].moving && move_to[0].path_index > 0) {
			fog_show_neighbors(move_to[0].path[move_to[0].path_index - 1], distance);
		}
	}
}

function fog_show_neighbors(hex, distance) {
	hex.fog.visible = false;
	ds_map_set(hex.fog.already_seen, ObjMatch.player, true);
	var neighbors = hex_neighbors(hex);
	for (var i = 0; i < array_length(neighbors); i++) {
		var position = hex_cube_to_offset(neighbors[i]);
		ObjMap.fog[position.x, position.y].visible = false;
		ds_map_set(ObjMap.fog[position.x, position.y].already_seen, ObjMatch.player, true);
		if (distance > 1) {
			fog_show_neighbors(neighbors[i], distance - 1);
		}
	}
}
