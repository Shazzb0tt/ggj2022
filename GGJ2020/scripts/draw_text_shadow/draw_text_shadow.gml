function draw_text_shadow(x, y, text, color = c_white) {
	draw_text_color(x + 1, y - 1, text, c_black, c_black, c_black, c_black, image_alpha);
	draw_text_color(x, y, text, color, color, color, color, image_alpha);
}