function hex_neighbors(hex) {
    var neighbors = [];
    for (var hex_direction = 0; hex_direction < 6; hex_direction++) {
        var neighbor = hex_neighbor(hex, hex_direction);
		var x_y_position = hex_cube_to_offset(neighbor);
		if (x_y_position.x >= 0
			&& x_y_position.y >= 0
			&& x_y_position.x < ObjMap.map_width
			&& x_y_position.y < ObjMap.map_height
		) {
			array_push(neighbors, ObjMap.tiles[x_y_position.x, x_y_position.y]);
		}
    }
    return neighbors;
}

function hex_offset_to_cube(x, y) {
	return {	
        q: x,
        r: y - (x + HexOffset.ODD * (x & 1)) / 2,
        s: -q - r,
	};
}

function hex_cube_to_offset(hex) {
	return {
        x: hex.q,
        y: hex.r + (hex.q + HexOffset.ODD * (hex.q & 1)) / 2,
	};
}

global.hex_directions = [
    { q: 1, r: 0, s: -1 },
    { q: 1, r: -1, s: 0 },
    { q: 0, r: -1, s: 1 },
    { q: -1, r: 0, s: 1 },
    { q: -1, r: 1, s: 0 },
    { q: 0, r: 1, s: -1 },
];

function hex_position_add(a, b) {
    return { q: a.q + b.q, r: a.r + b.r, s: a.s + b.s };
}

function hex_position_subtract(a, b) {
    return { q: a.q - b.q, r: a.r - b.r, s: a.s - b.s };
}

function hex_neighbor(hex, direction) {
    return hex_position_add(hex, global.hex_directions[direction]);
}

function hex_len(hex) {
    return (abs(hex.q) + abs(hex.r) + abs(hex.s)) / 2;
}
	
function hex_distance(a, b) {
    return hex_len(hex_position_subtract(a, b));
}

function hex_path(from_hex, to_hex, max_length) {
	var frontier = ds_priority_create();
	ds_priority_add(frontier, from_hex, 0);
	
    var came_from = ds_map_create();
	ds_map_add(came_from, from_hex, from_hex);
	
    var cost_so_far = ds_map_create();
	ds_map_add(cost_so_far, from_hex, 0);
		
    while (ds_priority_size(frontier) > 0) {
        var current = ds_priority_delete_min(frontier);
        if (current == to_hex) {
            break;
        }
		
		var neighbors = hex_neighbors(current);
        for (var i = 0; i < array_length(neighbors); i++) {
			var next = neighbors[i];
            var new_cost = ds_map_find_value(cost_so_far, current) + hex_path_cost(current, next);
			if (new_cost >= 9999) {
				continue;
			}
			var has_unit = find_unit_on_hex(next) != noone
			if (has_unit) {
				continue;
			}
			
            if (!ds_map_exists(cost_so_far, next) || new_cost < ds_map_find_value(cost_so_far, next)) {
				ds_map_set(cost_so_far, next, new_cost);
                var priority = new_cost + hex_distance(next, to_hex);
				ds_priority_add(frontier, next, priority);
				ds_map_set(came_from, next, current);
			}
		}
	}
	
	var path = hex_reconstruct_path(came_from, from_hex, to_hex, max_length);
	
	ds_priority_destroy(frontier);
	ds_map_destroy(came_from);
	ds_map_destroy(cost_so_far);
	
	return path;
}

function hex_path_cost(a, b) {
	return get_movement_cost(b);
}

function hex_reconstruct_path(came_from, from_hex, to_hex, max_length) {
    var current = to_hex;
    var path = [];
    while (current != from_hex) {
		array_push(path, current);
		if (!ds_map_exists(came_from, current)) {
			return [];
		}
        current = ds_map_find_value(came_from, current);
	}
	array_push(path, from_hex);
	var reverse = [];
	show_debug_message("max_length " + string(max_length) + " " + string(array_length(path)))
	for (var i = min(array_length(path), max_length); i > 0; i--) {
		array_push(reverse, path[i - 1]);
	}
	show_debug_message("max_lengt2 " + string(array_length(reverse)))
    return reverse;
}
