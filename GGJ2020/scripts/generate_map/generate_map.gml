// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

enum HexType {
	GRASS = 0,
	WOODS = 1,
	MOUNTAIN = 2,
	OCEAN = 3,
	DESERT = 4,
	DESERT2 = 5,
	DESERT_MOUNTAIN = 7,
};

function get_movement_cost(hex) {
	switch(hex.hex_type) {
		case HexType.GRASS: return 1;
		case HexType.WOODS: return 2;
		case HexType.MOUNTAIN: return 3;
		case HexType.OCEAN: return 9999;
		case HexType.DESERT: return 1;
		case HexType.DESERT2: return 2;
		case HexType.DESERT_MOUNTAIN: return 3;
	}
}

enum HexOffset {
    EVEN = 1,
    ODD = -1,
}

function generate_map() {
	show_debug_message("Generate map");
	ObjMap.x_size = 94;
	ObjMap.y_size = 105;
	ObjMap.map_width = 21;
	ObjMap.map_height = 10;
	
	ObjMap.tiles = [];
	ObjMap.fog = [];

	add_hex_group(0, 0, ObjMap.map_width, ObjMap.map_height, HexType.OCEAN)
	hex_add(2, 2, HexType.GRASS)
	hex_add(3, 2, HexType.GRASS)
	hex_add(2, 3, HexType.GRASS)
	hex_add(1, 2, HexType.GRASS)
	hex_add(2, 4, HexType.GRASS)
	hex_add(2, 5, HexType.WOODS)
	hex_add(1, 5, HexType.WOODS)
	hex_add(2, 6, HexType.WOODS)
	hex_add(2, 7, HexType.WOODS)
	hex_add(1, 7, HexType.WOODS)
	hex_add(2, 8, HexType.WOODS)
	hex_add(3, 3, HexType.WOODS)
	hex_add(3, 4, HexType.WOODS)
	hex_add(4, 2, HexType.GRASS)
	hex_add(5, 2, HexType.WOODS)
	hex_add(6, 3, HexType.WOODS)
	hex_add(7, 3, HexType.GRASS)
	hex_add(8, 4, HexType.GRASS)
	hex_add(8, 5, HexType.GRASS)
	hex_add(9, 3, HexType.GRASS)
	hex_add(3, 8, HexType.GRASS)
	hex_add(4, 8, HexType.WOODS)
	hex_add(5, 8, HexType.WOODS)
	hex_add(6, 8, HexType.WOODS)
	hex_add(7, 7, HexType.GRASS)
	hex_add(8, 8, HexType.GRASS)
	hex_add(9, 5, HexType.GRASS)
	hex_add(9, 7, HexType.GRASS)
	hex_add(10, 6, HexType.GRASS)
	hex_add(10, 7, HexType.GRASS)
	hex_add(4, 3, HexType.GRASS)
	hex_add(4, 4, HexType.GRASS)
	hex_add(4, 5, HexType.GRASS)
	hex_add(3, 5, HexType.GRASS)
	hex_add(3, 6, HexType.GRASS)
	hex_add(3, 7, HexType.GRASS)
	hex_add(4, 7, HexType.WOODS)
	hex_add(4, 6, HexType.GRASS)
	hex_add(5, 3, HexType.GRASS)
	hex_add(5, 4, HexType.WOODS)
	hex_add(5, 5, HexType.WOODS)
	hex_add(5, 6, HexType.GRASS)
	hex_add(5, 7, HexType.WOODS)
	hex_add(6, 4, HexType.GRASS)
	hex_add(6, 5, HexType.GRASS)
	hex_add(6, 6, HexType.GRASS)
	hex_add(6, 7, HexType.GRASS)
	hex_add(7, 4, HexType.MOUNTAIN)
	hex_add(7, 5, HexType.WOODS)
	hex_add(7, 6, HexType.WOODS)
	hex_add(8, 6, HexType.WOODS)
	hex_add(8, 7, HexType.MOUNTAIN)
	hex_add(9, 6, HexType.GRASS)
	hex_add(18, 2, HexType.DESERT)
	hex_add(17, 2, HexType.DESERT)
	hex_add(16, 3, HexType.DESERT)
	hex_add(15, 3, HexType.DESERT)
	hex_add(14, 4, HexType.DESERT)
	hex_add(13, 4, HexType.DESERT_MOUNTAIN)
	hex_add(12, 5, HexType.DESERT)
	hex_add(11, 5, HexType.DESERT)
	hex_add(17, 3, HexType.DESERT2)
	hex_add(17, 4, HexType.DESERT2)
	hex_add(17, 6, HexType.DESERT)
	hex_add(18, 6, HexType.DESERT)
	hex_add(17, 7, HexType.DESERT)
	hex_add(17, 8, HexType.DESERT)
	hex_add(16, 8, HexType.DESERT2)
	hex_add(15, 8, HexType.DESERT2)
	hex_add(14, 8, HexType.DESERT2)
	hex_add(12, 8, HexType.DESERT)
	hex_add(11, 7, HexType.DESERT)
	hex_add(10, 8, HexType.DESERT)
	hex_add(12, 4, HexType.DESERT)
	hex_add(11, 3, HexType.DESERT)
	hex_add(15, 3, HexType.DESERT)
	hex_add(14, 3, HexType.DESERT2)
	hex_add(13, 3, HexType.DESERT)
	hex_add(15, 2, HexType.DESERT2)
	hex_add(16, 2, HexType.DESERT)
	hex_add(16, 4, HexType.DESERT)
	hex_add(15, 4, HexType.DESERT2)
	hex_add(14, 5, HexType.DESERT)
	hex_add(13, 5, HexType.DESERT2)
	hex_add(12, 6, HexType.DESERT2)
	hex_add(11, 6, HexType.DESERT)
	hex_add(12, 7, HexType.DESERT_MOUNTAIN)
	hex_add(13, 7, HexType.DESERT)
	hex_add(13, 6, HexType.DESERT2)
	hex_add(14, 6, HexType.DESERT)
	hex_add(15, 5, HexType.DESERT2)
	hex_add(16, 5, HexType.DESERT)
	hex_add(14, 7, HexType.DESERT)
	hex_add(15, 6, HexType.DESERT)
	hex_add(16, 6, HexType.DESERT)
	hex_add(17, 5, HexType.DESERT)
	hex_add(15, 7, HexType.DESERT2)
	hex_add(16, 7, HexType.DESERT2)
	hex_add(18, 8, HexType.DESERT2)
	hex_add(19, 2, HexType.DESERT)
	hex_add(18, 3, HexType.DESERT)
	hex_add(18, 4, HexType.DESERT)
	hex_add(18, 5, HexType.DESERT2)
	hex_add(18, 6, HexType.DESERT2)
	hex_add(19, 5, HexType.DESERT2)
	hex_add(18, 7, HexType.DESERT2)
	hex_add(19, 7, HexType.DESERT2)
}

function get_x_pixels(x_index) {
	return x_index * ObjMap.x_size;
}

function get_y_pixels(x_index, y_index) {
	var y_offset = 0;
	if (x_index % 2 == 1) {
		y_offset = ObjMap.y_size * 0.5;
	}
	
	return y_index * ObjMap.y_size + y_offset
}

function get_depth(x_index, y_index) {
	var hex_depth = 5000 + -(y_index * 2);
	if (x_index % 2 == 1) {
		hex_depth--;
	}
	
	return hex_depth;
}

function get_unit_depth(unit) {
	return 3000 - unit.y;
}

function hex_add(x_hex, y_hex, type) {
	if (array_height_2d(ObjMap.tiles) > x_hex && array_length_2d(ObjMap.tiles, x_hex) > y_hex) {
        instance_destroy(ObjMap.tiles[x_hex, y_hex]);
    }
	
    var inst = instance_create_layer(get_x_pixels(x_hex), get_y_pixels(x_hex, y_hex), "Hexes", ObjHex);
	inst.depth = get_depth(x_hex, y_hex);
	inst.image_speed = 0;
	inst.x_hex = x_hex;
	inst.y_hex = y_hex;
	inst.hex_type = type;
	inst.image_index = type

	inst.q = x_hex;
    inst.r = y_hex - (x_hex + HexOffset.ODD * (x_hex & 1)) / 2;
    inst.s = -inst.q - inst.r;
	
	ObjMap.tiles[x_hex, y_hex] = inst;
	
	var fog;
	if (array_height_2d(ObjMap.fog) > x_hex && array_length_2d(ObjMap.fog, x_hex) > y_hex) {
        fog = ObjMap.fog[x_hex, y_hex];
    } else {
		fog = instance_create_depth(get_x_pixels(x_hex), get_y_pixels(x_hex, y_hex), 10, ObjFog);
		ObjMap.fog[x_hex, y_hex] = fog;	
	}
	inst.fog = fog;
	
	// Decals
	if (type == HexType.GRASS) {
		var amount = round(random_range(1, 5));
		for (var a = 0; a < amount; a++) {
			var decal_position = find_free_decal_position(inst.x, inst.y, 50, 10);
			if (decal_position.found) {
				var decal = instance_create_layer(decal_position.x, decal_position.y, "Instances", ObjGrass);
				decal.depth = get_unit_depth(decal);
				decal.image_index = random_range(0, sprite_get_number(decal.sprite_index));
				decal.hex = inst;
			}
		}
		var amount = round(random_range(0, 1));
		for (var a = 0; a < amount; a++) {
			var decal_position = find_free_decal_position(inst.x, inst.y, 50, 10);
			if (decal_position.found) {
				var decal = instance_create_layer(decal_position.x, decal_position.y, "Instances", ObjTree);
				decal.depth = get_unit_depth(decal);
				decal.image_index = random_range(0, sprite_get_number(decal.sprite_index));
				decal.hex = inst;
			}
		}
	}
	if (type == HexType.WOODS) {
		var amount = round(random_range(15, 25));
		for (var a = 0; a < amount; a++) {
			var decal_position = find_free_decal_position(inst.x, inst.y, 59, 3);
			if (decal_position.found) {
				var decal = instance_create_layer(decal_position.x, decal_position.y, "Instances", ObjTree);
				decal.depth = get_unit_depth(decal);
				decal.image_index = random_range(0, sprite_get_number(decal.sprite_index));
				decal.hex = inst;
			}
		}
	}
	if (type == HexType.DESERT) {
		var amount = round(random_range(0, 1));
		for (var a = 0; a < amount; a++) {
			var decal_position = find_free_decal_position(inst.x, inst.y, 59, 3);
			if (decal_position.found) {
				var decal = instance_create_layer(decal_position.x, decal_position.y, "Instances", ObjBush);
				decal.depth = get_unit_depth(decal);
				decal.image_index = random_range(0, sprite_get_number(decal.sprite_index));
				decal.hex = inst;
			}
		}
	}
	if (type == HexType.DESERT2) {
		var amount = round(random_range(10, 20));
		for (var a = 0; a < amount; a++) {
			var decal_position = find_free_decal_position(inst.x, inst.y, 59, 10);
			if (decal_position.found) {
				var decal = instance_create_layer(decal_position.x, decal_position.y, "Instances", ObjTree2);
				decal.depth = get_unit_depth(decal);
				decal.image_index = random_range(0, sprite_get_number(decal.sprite_index));
				decal.hex = inst;
			}
		}
	}
}

function find_free_decal_position(hex_x, hex_y, range, distance_limit) {
	var bail = 20;
	while (true) {
		var distance = random_range(0, range);
		var decal_direction = random_range(0, 359);
		var decal_x = hex_x + lengthdir_x(distance, decal_direction);
		var decal_y = hex_y + lengthdir_y(distance, decal_direction);
		var near = instance_nearest(decal_x, decal_y, ObjDecal);
		if (near == noone || point_distance(decal_x, decal_y, near.x, near.y) >= distance_limit) {
			return {
				found: true,
				x: decal_x,
				y: decal_y,
			};
		}
		bail--;
		if (bail <= 0) {
			return {
				found: false,
			};
		}
	}
}

function add_hex_group(x_start, y_start, x_end, y_end, type) {
	for (hexY = y_start; hexY < y_end; hexY++) {
		for (x = x_start; x < x_end; x++) {
			hex_add(x, hexY, type);
		}
	}
}