function find_unit_on_hex(_hex) {
	with (ObjCastle) {
		if (_hex.x_hex = hex.x_hex && _hex.y_hex = hex.y_hex) {
			return id;
		}
	}
	with (ObjTown) {
		if (_hex.x_hex = hex.x_hex && _hex.y_hex = hex.y_hex) {
			return id;
		}
	}
	with (ObjUnit) {
		if (_hex.x_hex = hex.x_hex && _hex.y_hex = hex.y_hex) {
			return id;
		}
	}
	return noone;
}

function get_hex_unit_offset(hex) {
	var result_x = hex.x;
	var result_y = hex.y;
	var unit_on_hex = find_unit_on_hex(hex);
	if (unit_on_hex != noone) {
		if (unit_on_hex.object_index == ObjCastle) {
			result_y += 40;
		} else if (unit_on_hex.object_index == ObjTown) {
			result_y += 30;
		} else if (hex.hex_type == HexType.MOUNTAIN) {
			result_y -= 30;
		} else if (hex.hex_type == HexType.DESERT_MOUNTAIN) {
			result_y -= 20;
		} else {
			//result_y += 20;
		}
	}
	return {
		x: result_x,
		y: result_y,
	};
}
