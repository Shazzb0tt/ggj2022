function turn_end() {	
	show_debug_message("Turn end");
	
	if (cant_end_turn()) {
		return
	}
	
	ObjBuildMenu.visible = false;
	ObjOverlay.visible = true;
		
	ObjMatch.current_player++;
	var distribute_funds = false;
	if (ObjMatch.current_player >= array_length(ObjMatch.players)) {
		ObjMatch.current_player = 0;
		ObjMatch.turn++;
		distribute_funds = true;
	}
	
	ObjMatch.player = ObjMatch.players[ObjMatch.current_player];
	ObjMatch.player.selected = [];
	ObjMatch.player.moving = false;
	
	var last_unit = noone;
	with (ObjCastle) {
		if (player == ObjMatch.player) {
			last_unit = id;
		}
		if (distribute_funds && player != noone) {
			player.money += 50;
		}
	}
	with (ObjTown) {
		if (player == ObjMatch.player) {
			last_unit = id;
		}
		if (distribute_funds && player != noone) {
			player.money += 30;
		}
	}
	
	with (ObjUnit) {
		if (player == ObjMatch.player) {
			move_to = [];
			turn_movement_count = stats.movement_count;
			has_attacked = false;
			last_unit = id;
		}
		//if (array_length(move_to) > 0) {
		//	move_to[0].moving = false;
		//}
		move_to = [];
	}
	
	if (last_unit != noone) {
		var new_x = clamp(last_unit.x - camera_get_view_width(view_camera[0]) / 2, 0, 1350);
		var new_y = clamp(last_unit.y - camera_get_view_height(view_camera[0]) / 2, 20, 150);
		camera_set_view_pos(view_camera[0], new_x, new_y);
	}
	
	with (ObjFog) {
		visible = true;
		if (ds_map_find_value(already_seen, ObjMatch.player)) {
			image_alpha = 0.5;
		} else {
			image_alpha = 1;
		}
	}
}

function turn_continue() {
	ObjMatch.player.moving = false;
	with (ObjUnit) {
		if (array_length(move_to) > 0) {
			move_to[0].moving = true;
		}
	}
}

function cant_end_turn() {
	if (ObjMatch.current_player >= 0) {
		with (ObjUnit) {
			if (array_length(move_to) > 0 && move_to[0].moving && turn_movement_count > 0) {
				return true;
			}
		}
	}
	
	return false;
}
