event_inherited()

if (cant_end_turn()) {
	image_alpha = 0.5;
} else {
	image_alpha = 1.0;
}