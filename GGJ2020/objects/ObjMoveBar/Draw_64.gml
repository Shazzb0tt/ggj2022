draw_self();

draw_sprite(icon, 0, x + 5, y + 2)
var max_bar_width = 105;
draw_sprite_stretched(SprBarDarkBg, 0, x + 5 + 2 + sprite_get_width(SprIconHeart), y + 5, max_bar_width, sprite_get_height(SprBarBlueBg) + 2);
if (array_length(ObjMatch.player.selected) > 0) {
	var bar_width = ObjMatch.player.selected[0].turn_movement_count / ObjMatch.player.selected[0].stats.movement_count * (max_bar_width - 2);
	draw_sprite_stretched(SprBarBlueBg, 0, x + 5 + 2 + sprite_get_width(SprIconHeart) + 1, y + 5 + 1, bar_width, sprite_get_height(SprBarBlueBg));
}
