event_inherited();
image_scale = random_range(0.3, 0.5);
image_xscale = image_scale;
image_yscale = image_scale;
