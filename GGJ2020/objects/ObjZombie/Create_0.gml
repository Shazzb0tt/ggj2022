event_inherited()
stats.hp = 8;
stats.movement_count = global.debug ? 200 : 2;
stats.melee_damage = 5;
image_scale = 0.2;
image_xscale = image_scale;
image_yscale = image_scale;
death_sound = SndZombieDeath;
audio_play_sound(SndZombieSpawn, 1, false);
