event_inherited();
image_scale = 0.2;
stats.hp = 6;
stats.movement_count = global.debug ? 200 : 3;
stats.melee_damage = 5;
image_xscale = image_scale;
image_yscale = image_scale;
death_sound = SndSkeletonDeath;
audio_play_sound(SndSkeletonSpawn, 1, false);