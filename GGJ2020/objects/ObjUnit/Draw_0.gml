if (array_length(move_to) > 0 && move_to[0].moving && turn_movement_count > 0) {
	if (xprevious < x) {
		image_xscale = image_scale;
	} else {
		image_xscale = -image_scale;
	}
	draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);	
} else {
	draw_sprite_ext(sprite_index, 0, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);	
}

for (var i = 0; i < array_length(move_to); i++) {
	var final_position = get_hex_unit_offset(move_to[i].hex);
	draw_sprite(SprMoveTo, 0, final_position.x, final_position.y);
		
	if (global.debug) {
		for (var p = 0; p < array_length(move_to[i].path) - 1; p++) {
			var current_position = get_hex_unit_offset(move_to[i].path[p]);
			var next_position = get_hex_unit_offset(move_to[i].path[p + 1]);
			draw_line_color(current_position.x, current_position.y, next_position.x, next_position.y, c_blue, c_blue);
		}
	}
}

if (global.debug) {
	draw_text(x + 10, y, string(turn_movement_count));
}
