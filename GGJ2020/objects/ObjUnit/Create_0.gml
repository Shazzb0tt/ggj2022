move_to = [];
image_scale = 1;
attack_animation = ObjSlash;

stats = {
	movement_count: 4,
	attack_range: 1,
	hp: 10,
	melee_damage: 5,
	range_damage: 4
}

has_attacked = false;