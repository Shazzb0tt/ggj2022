depth = get_unit_depth(id);

if (array_length(move_to) > 0 && move_to[0].moving) {
	var final_position = get_hex_unit_offset(move_to[0].hex);
	if (point_distance(x, y, final_position.x, final_position.y) > 3) {
		var next_position = get_hex_unit_offset(move_to[0].path[move_to[0].path_index]);
		hex = move_to[0].path[move_to[0].path_index];
		var movement_cost = get_movement_cost(hex);
		if (point_distance(x, y, next_position.x, next_position.y) > 3) {
			speed = 2/movement_cost;
			direction = point_direction(x, y, next_position.x, next_position.y);
		} else {
			move_to[0].path_index++;
			if (move_to[0].path_start) {
				move_to[0].path_start = false;
			} else {
				turn_movement_count -= movement_cost;
			}
		}
	} else {
		var movement_cost = get_movement_cost(move_to[0].hex);
		turn_movement_count -= movement_cost;
		speed = 0;
		hex = move_to[0].hex;
		array_delete(move_to, 0, 1);
	}
}

if (turn_movement_count <= 0) {
	speed = 0;
}