event_inherited();
stats.hp = 12;
stats.movement_count = global.debug ? 200 : 4;
stats.melee_damage = 6;
image_scale = 0.3;
image_xscale = image_scale;
image_yscale = image_scale;
death_sound = SndPikemanDeath;
audio_play_sound(SndPikemanSpawn, 1, false);