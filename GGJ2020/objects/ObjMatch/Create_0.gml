generate_map();

players = [];
players[0] = instance_create_depth(100, 0, 0, ObjPlayer);
players[0].team = Team.LIGHT;
players[1] = instance_create_depth(150, 0, 0, ObjPlayer);
players[1].team = Team.DARK;

unit_add_castle(2, 2, players[0]);
unit_add_neutral_castle(11, 3);
unit_add_neutral_castle(4, 8);

unit_add_castle(18, 2, players[1]);
unit_add_neutral_castle(9, 3);
unit_add_neutral_castle(16, 8);

unit_add_neutral_town(17, 5);
unit_add_neutral_town(3, 5);
unit_add_neutral_town(10, 7);

turn = 0;
current_player = -1;
turn_end();
