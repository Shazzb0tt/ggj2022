event_inherited();
stats.hp = 15;
stats.movement_count = global.debug ? 200 : 4;
stats.melee_damage = 5;
image_scale = 0.3;
image_xscale = image_scale;
image_yscale = image_scale;
death_sound = SndKnightDeath;
audio_play_sound(SndKnightSpawn, 1, false);