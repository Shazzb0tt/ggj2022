draw_self();

if (global.debug) {
	draw_set_font(FntDebug);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	var text_alpha = 0.5;
	var x_y_position = string(round(x)) + " " + string(round(y)) + " " + string(round(depth));
	draw_text_color(x, y + 5, x_y_position, c_white, c_white, c_white, c_white, text_alpha);
}
