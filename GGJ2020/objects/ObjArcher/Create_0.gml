event_inherited()
attack_animation = ObjArrow;
stats.movement_count = global.debug ? 200 : 3;
stats.attack_range = 2;
stats.hp = 8;
image_scale = 0.2;
image_xscale = image_scale;
image_yscale = image_scale;
death_sound = SndArcherDeath;
audio_play_sound(SndArcherSpawn, 1, false);