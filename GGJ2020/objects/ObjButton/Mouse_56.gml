if (button_width) {
	if (button_clicked_position(x, y, x + button_width, y + sprite_height)) {
		show_debug_message("Click button");
		event_user(0);
	}
} else {
	draw_set_font(FntHeading);
	if (button_clicked_position(x, y, x + string_width(text) + 40, y + sprite_height)) {
		show_debug_message("Click button");
		event_user(0);
	}
}