draw_set_alpha(image_alpha)
draw_set_valign(fa_middle);
draw_set_font(FntHeading);
if (button_width) {
	draw_set_halign(fa_center);
	if (center) {
		x = (global.gui_width / 2) - (button_width / 2);
	}
	draw_sprite_stretched(sprite_index, 0, x, y, button_width, sprite_height);
	draw_text_shadow(x + button_width / 2, y + sprite_height / 2, text);
	if (global.debug) {
		draw_rectangle(x, y, x + button_width, y + sprite_height, true);
	}
} else {
	draw_set_halign(fa_left);
	draw_sprite_stretched(sprite_index, 0, x, y, string_width(text) + 40, sprite_height);
	draw_text_shadow(x + 20, y + sprite_height / 2, text);
	if (global.debug) {
		draw_rectangle(x, y, x + string_width(text) + 40, y + sprite_height, true);
	}
}
draw_set_alpha(1)