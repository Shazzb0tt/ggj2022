image_xscale = 1;
image_yscale = 1;
opening = false;

x = (global.gui_width / 2) - (sprite_width / 2);
y = (global.gui_height / 2) - (sprite_height / 2);

unit_list_dark = [];
array_push(unit_list_dark, {
	name: "Skelly",
	sprite: SprSkelly2,
	sprite_scale: 0.2,
	unit: ObjSkelly,
	cost: 75,
	stats: [
		"$ 75",
		"    Boney",
	],
});

array_push(unit_list_dark, {
	name: "Zombie",
	sprite: SprZombie,
	sprite_scale: 0.2,
	unit: ObjZombie,
	cost: 75,
	stats: [
		"$ 75",
		"        Infectious",
	],
});

array_push(unit_list_dark, {
	name: "Ogre",
	sprite: SprOgre,
	sprite_scale: 0.2,
	unit: ObjOgre,
	cost: 300,
	stats: [
		"$ 300",
		"     Big Boy",
	]
});

unit_list_light = [];

array_push(unit_list_light, {
	name: "Pikeman",
	sprite: SprPikeman,
	sprite_scale: 0.25,
	unit: ObjPikeman,
	cost: 100,
	stats: [
		"$ 100",
		"      Common",
	],
});

array_push(unit_list_light, {
	name: "Archer",
	sprite: SprArcher2,
	sprite_scale: 0.25,
	unit: ObjArcher,
	cost: 150,
	stats: [
		"$ 150",
		"    Ranged",
	],
});

array_push(unit_list_light, {
	name: "Knight",
	sprite: SprKnightWalk2,
	sprite_scale: 0.28,
	unit: ObjKnight,
	cost: 175,
	stats: [
		"$ 175",
		"      Armored",
	],
});
