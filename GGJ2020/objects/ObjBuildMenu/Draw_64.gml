draw_set_alpha(0.6);
draw_rectangle_color(0, 0, global.gui_width, global.gui_height, c_black, c_black, c_black, c_black, false);
draw_set_alpha(1);

draw_self();

draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_font(FntHeading);
draw_text_color(x + sprite_width / 2 + 1, y + 33 - 1, "Build Menu", c_black, c_black, c_black, c_black, 1);
draw_text_color(x + sprite_width / 2, y + 33, "Build Menu", c_white, c_white, c_white, c_white, 1);

var unit_list;
if (ObjMatch.player.team == Team.LIGHT) {
	unit_list = unit_list_light;
} else if (ObjMatch.player.team == Team.DARK) {
	unit_list = unit_list_dark;
}
	
var y_offset = 55;
for (var i = 0; i < array_length(unit_list); i++) {
	draw_sprite(SprBaseBoard2, 0, x + 30, y + y_offset);

	draw_set_font(FntHeading);
	draw_text_color(x + 30 + sprite_get_width(SprBaseBoard2) / 2 + 1, y + y_offset + 21 - 1, unit_list[i].name, c_black, c_black, c_black, c_black, 1);
	draw_text_color(x + 30 + sprite_get_width(SprBaseBoard2) / 2, y + y_offset + 21, unit_list[i].name, c_white, c_white, c_white, c_white, 1);

	for (var t = 0; t < array_length(unit_list[i].stats); t++) {
		draw_text_color(x + 30 + 120 + 1, y + y_offset + 70 + (t * 25) - 1, unit_list[i].stats[t], c_black, c_black, c_black, c_black, 1);
		draw_text_color(x + 30 + 120, y + y_offset + 70 + (t * 25), unit_list[i].stats[t], c_white, c_white, c_white, c_white, 1);
	}

	draw_sprite_stretched(SprBoardBg, 0, x + 65, y + y_offset + 55, 55, 55);
		
	draw_sprite_ext(unit_list[i].sprite, 0, x + 65 + 25, y + y_offset + 55 + 50, unit_list[i].sprite_scale, unit_list[i].sprite_scale, 0, c_white, 1);
	
	draw_sprite(SprButtonIcon, 0, x + 230,  y + y_offset + 47);
	draw_set_font(FntIcon);
	draw_text_color(x + 230 + 36 + 1,  y + y_offset + 47 + 38 - 1, "+", c_black, c_black, c_black, c_black, 1);
	draw_text_color(x + 230 + 36,  y + y_offset + 47 + 38, "+", c_white, c_white, c_white, c_white, 1);
	
	if (global.debug) {
		draw_rectangle(x + 30, y + y_offset, x + 30 + 300, y + y_offset + 131, true);
	}
	
	y_offset += 150;
}

draw_set_font(FntBody);
draw_set_halign(fa_left);
draw_set_valign(fa_top);