if (opening) {
	opening = false;
	return;
}
if (visible) {
	show_debug_message("Build menu click");
	if (mouse_in_button(x, y, x + sprite_width, y + sprite_height)) {
		var unit_list;
		if (ObjMatch.player.team == Team.LIGHT) {
			unit_list = unit_list_light;
		} else if (ObjMatch.player.team == Team.DARK) {
			unit_list = unit_list_dark;
		}
		var y_offset = 55;
		for (var i = 0; i < array_length(unit_list); i++) {
			if (mouse_in_button(x + 30, y + y_offset, x + 30 + 300, y + y_offset + 131)) {
				show_debug_message("Click build button " + string(unit_list[i].unit));
				if (castle.player.money < unit_list[i].cost) {
					return
				}
				castle.player.money = castle.player.money - unit_list[i].cost
				unit_add(castle.hex, castle.player, unit_list[i].unit);
				ObjBuildMenu.visible = false;
				click();
			}
			y_offset += 150;
		}
	} else {
		show_debug_message("Hide build menu");
		ObjBuildMenu.visible = false;
		click();
	}
}
