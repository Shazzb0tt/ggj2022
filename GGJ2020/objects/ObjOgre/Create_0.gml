event_inherited();
stats.hp = 20;
stats.movement_count = global.debug ? 200 : 6;
stats.melee_damage = 10;
image_scale = 0.3;
image_xscale = image_scale;
image_yscale = image_scale;
death_sound = SndOgreDeath;
audio_play_sound(SndOgreSpawn, 1, false);