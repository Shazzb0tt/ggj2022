if (!neutral && player == ObjMatch.player) {
	var unit_on_top = noone;
	with (ObjUnit) {
		if (hex.x_hex == other.hex.x_hex && hex.y_hex == other.hex.y_hex) {
			unit_on_top = id;
		}
	}
	if (!unit_on_top) {
		show_debug_message("Show build menu " + string(player.team));
		ObjBuildMenu.castle = id;
		ObjBuildMenu.visible = true;
		ObjBuildMenu.opening = true;
		click();
	}
}
