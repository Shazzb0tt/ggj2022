event_inherited();
image_scale = random_range(0.2, 0.3);
image_xscale = image_scale;
image_yscale = image_scale;
