if (visible) {
	if (button_clicked_position(x, y, x + button_width, y + sprite_get_height(SprButtonIcon))) {
		show_debug_message("Click ready");
		visible = false;
		click();
	}
}
