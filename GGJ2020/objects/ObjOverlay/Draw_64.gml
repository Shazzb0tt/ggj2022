draw_set_alpha(0.6);
draw_rectangle_color(0, 0, global.gui_width, global.gui_height, c_black, c_black, c_black, c_black, false);
draw_set_alpha(1);
if (ObjMatch.player.team == Team.LIGHT) {
	draw_sprite(SprPlayerLabel, 0, global.gui_width / 2, global.gui_height / 2);
} else {
	draw_sprite(SprPlayerLabel, 1, global.gui_width / 2, global.gui_height / 2);
}

draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_set_font(FntHeading);
draw_sprite_stretched(SprButtonIcon, 0, x, y, button_width, sprite_get_height(SprButtonIcon));
draw_text_shadow(x + button_width / 2, y + sprite_get_height(SprButtonIcon) / 2, text);
if (global.debug) {
	draw_rectangle(x, y, x + button_width, y + sprite_get_height(SprButtonIcon), true);
}
