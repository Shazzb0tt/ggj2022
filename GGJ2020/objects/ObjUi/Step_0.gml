if (mouse_check_button(mb_middle)) {
	if (moving_camera) {
		var diff_x = mouse_x - moving_camera_x;
		var diff_y = mouse_y - moving_camera_y;
		var new_x = clamp(camera_get_view_x(view_camera[0]) - diff_x, 0, 1350);
		var new_y = clamp(camera_get_view_y(view_camera[0]) - diff_y, 20, 150);
		camera_set_view_pos(view_camera[0], new_x, new_y);
		moving_camera_x = mouse_x;
		moving_camera_y = mouse_y;
	} else {
		moving_camera = true;
		moving_camera_x = mouse_x;
		moving_camera_y = mouse_y;
	}
} else {
	moving_camera = false;
}
