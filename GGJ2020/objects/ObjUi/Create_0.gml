// Resize window
window_set_size(global.gui_width, global.gui_height);
camera_set_view_size(view_camera[0], global.gui_width, global.gui_height);
view_set_wport(0, global.gui_width);
view_set_hport(0, global.gui_height);
surface_resize(application_surface, global.gui_width, global.gui_height);

// Init vars
moving_camera = false;
image_speed = 0.1;

// Create UI
instance_create_depth(0, 0, -3, ObjOverlay);

instance_create_depth(0, 0, -1, ObjButtonNextTurn);
ObjButtonNextTurn.x = global.gui_width - 50 - 120;
ObjButtonNextTurn.y = global.gui_height - ObjButtonNextTurn.sprite_height - 14;

//instance_create_depth(0, 0, -1, ObjButtonContinue);
//ObjButtonContinue.x = 20 + ObjButtonNextTurn.sprite_width;
//ObjButtonContinue.y = gui_height - ObjButtonNextTurn.sprite_height - 10;

instance_create_depth(10, 10, -2, ObjBuildMenu);
ObjBuildMenu.visible = false;

var bar_margin = {
	x: 40,
	y: 18,
}
instance_create_depth(200, global.gui_height - sprite_get_height(SprBaseBoard) + bar_margin.y, -1, ObjHealthBar);
instance_create_depth(200, global.gui_height - sprite_get_height(SprBarBg) - bar_margin.y, -1, ObjMoveBar);
