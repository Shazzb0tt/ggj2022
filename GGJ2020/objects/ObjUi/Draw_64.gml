draw_sprite_stretched(SprBaseBoard, 0, 0, global.gui_height - 100, global.gui_width, 100);

draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(FntDebug);

if (global.debug) {
	if (ObjMatch) {
		var textX = 10;
		var textY = 10;
	
		draw_text(textX, textY, "Turn " + string(ObjMatch.turn));

		textY += 20;
		draw_text(textX, textY, "Camera " + string(camera_get_view_x(view_camera[0])) + " " + string(camera_get_view_y(view_camera[0])));

		textY += 20;
		draw_text(textX, textY, "Current Player " + string(ObjMatch.current_player + 1));

		textY += 20;
		draw_text(textX, textY, "Players " + string(array_length(ObjMatch.players)));
	}

	for (var i = 0; i < array_length(ObjMatch.players); i++) {
		textY += 20;
		draw_text(textX, textY, "Player " + string(i + 1));
	
		textY += 20;
		draw_text(textX, textY, "  Money " + string(ObjMatch.players[i].money));
	}

} 

draw_sprite(SprIconMoney, 0, 30, global.gui_height - sprite_get_height(SprBaseBoard) / 2);
draw_sprite_stretched(SprBoardBg, 0, 30 + sprite_get_width(SprIconMoney) + 10, global.gui_height - sprite_get_height(SprBaseBoard) + 18, 80, sprite_get_height(SprBaseBoard) - 18 * 2);

draw_set_font(FntMoney)
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_text_shadow(30 + sprite_get_width(SprIconMoney) + 10 + 40, global.gui_height - 50, string(ObjMatch.player.money), c_yellow);
