draw_self();

if (global.debug) {
	draw_set_font(FntDebug);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	var text_alpha = 0.5;
	draw_text_color(x, y - 15, string(id), c_white, c_white, c_white, c_white, text_alpha);

	var x_y_position = string(x_hex) + " " + string(y_hex) + " " + string(depth);
	draw_text_color(x, y + 5, x_y_position, c_white, c_white, c_white, c_white, text_alpha);

	draw_text_color(x, y - 45, string(q), c_white, c_white, c_white, c_white, text_alpha);
	draw_text_color(x + 35, y + 15, string(r), c_white, c_white, c_white, c_white, text_alpha);
	draw_text_color(x - 35, y + 15, string(s), c_white, c_white, c_white, c_white, text_alpha);
}
