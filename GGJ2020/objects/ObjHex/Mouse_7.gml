if (ObjBuildMenu.visible || ObjOverlay.visible) {
	return;
}

show_debug_message("Clicked hex " + string(id) + " " + string(x_hex) + " " + string(y_hex));

var overlapping = ds_list_create();
var numerOfOverlapping = instance_position_list(mouse_x, mouse_y, ObjHex, overlapping, true);
// show_debug_message("Overlapping " + string(numerOfOverlapping) + " " + string(ds_list_find_value(overlapping, 0)));

if (ds_list_find_value(overlapping, 0) == id) {
	var current_hex = id
	var neighbors = hex_neighbors(id);
	
	// Attack Unit
	with (ObjUnit) {
		if (player != ObjMatch.player
				&& hex == current_hex
				&& array_length(ObjMatch.player.selected) > 0) {
			var selected_unit = ObjMatch.player.selected[0];
			var distance = hex_distance(selected_unit.hex, current_hex);
			if ((distance) <= selected_unit.stats.attack_range
					&& selected_unit.has_attacked == false) {
				// Attack
				var damage = 0;
				if (distance > 1) {
					damage = selected_unit.stats.range_damage;
				} else {
					damage = selected_unit.stats.melee_damage;
				}
				
				current_hp -= damage;
				if (current_hp <= 0) {
					current_hp = 0;
					var temp_hex = hex;
					instance_destroy();
					if (selected_unit.object_index == ObjZombie) {
						var zombie = unit_add(temp_hex, selected_unit.player, ObjZombie)
						zombie.turn_movement_count = 0;
						zombie.has_attacked = true;
					}
				}
				selected_unit.has_attacked = true;
				selected_unit.turn_movement_count = 0;
				instance_create_depth(current_hex.x, current_hex.y, 1, selected_unit.attack_animation);
				return;
			}
			
			if (selected_unit.has_attacked == true) {
				return;
			}
		}
	}
	
	// Attack Town
	if (array_length(ObjMatch.player.selected) > 0) {
		with (ObjTown) {
			if (((!neutral && player != ObjMatch.player) || neutral)
				&& hex == current_hex
			) {
				var selected_unit = ObjMatch.player.selected[0]
				var distance = hex_distance(selected_unit.hex, current_hex)
				if (distance == 1 && selected_unit.has_attacked == false) {
					  current_hp = current_hp - selected_unit.stats.melee_damage
					  selected_unit.has_attacked = true;
					  selected_unit.turn_movement_count = 0;
					  instance_create_depth(current_hex.x, current_hex.y, 1, selected_unit.attack_animation);
					  if (current_hp <= 0 && !is_undefined(hex)) {
						  var x_pos = hex.x_hex;
						  var y_pos = hex.y_hex;
						  var temp_player = ObjMatch.player;
						  instance_destroy();
						  unit_add_town(x_pos, y_pos, temp_player);
					  }
					  return;
				}
				
				if (selected_unit.has_attacked == true) {
					return;
				}
			}
		}
	}
	
	// Attack Castle
	if (array_length(ObjMatch.player.selected) > 0) {
		with (ObjCastle) {
			if (((!neutral && player != ObjMatch.player) || neutral)
				&& hex == current_hex
			) {
				var selected_unit = ObjMatch.player.selected[0]
				var distance = hex_distance(selected_unit.hex, current_hex)
				if (distance == 1 && selected_unit.has_attacked == false) {
					  current_hp = current_hp - selected_unit.stats.melee_damage
					  selected_unit.has_attacked = true
					  selected_unit.turn_movement_count = 0;
					  instance_create_depth(current_hex.x, current_hex.y, 1, selected_unit.attack_animation);
					  if (current_hp <= 0 && !is_undefined(hex)) {
						  current_hp = 0;
						  var x_pos = hex.x_hex;
						  var y_pos = hex.y_hex;
						  var was_neutral = neutral;
						  var old_owner = player;
						  var temp_player = ObjMatch.player;
						  instance_destroy();
						  unit_add_castle(x_pos, y_pos, temp_player);
						  
						  if (!was_neutral) {
							  if (has_player_lost(old_owner)) {
								  room_goto(RooMenu);
							  }
						  }
					  }
					  return;
				}
				
				if (selected_unit.has_attacked == true) {
					return;
				}
			}
		}
	}
	
	// Set move unit
	if (array_length(ObjMatch.player.selected) > 0) {
		show_debug_message("Moving selected units " + string(array_length(ObjMatch.player.selected)) + " to " + string(x_hex) + " " + string(y_hex) + " / " + string(x) + " " + string(y));
		for (var i = 0; i < array_length(ObjMatch.player.selected); i++) {
			if (ObjMatch.player.selected[i].hex == current_hex) {
				continue;
			}
			var path = hex_path(ObjMatch.player.selected[i].hex, id, ObjMatch.player.selected[i].turn_movement_count);
			if (array_length(path) > 0 && array_length(ObjMatch.player.selected[i].move_to) == 0) {
				var last_hex = array_pop(path);
				array_push(path, last_hex);
				ObjMatch.player.selected[i].move_to = [{
					hex: last_hex,
					path: path,
					path_index: 0,
					path_start: true,
					moving: true,
				}];
			}
		}
	}
}
