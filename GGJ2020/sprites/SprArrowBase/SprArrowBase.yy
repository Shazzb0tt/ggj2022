{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 191,
  "bbox_top": 0,
  "bbox_bottom": 190,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 192,
  "height": 192,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7315ad1f-c321-4fea-b36d-72515594faf1","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7315ad1f-c321-4fea-b36d-72515594faf1","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"7315ad1f-c321-4fea-b36d-72515594faf1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bb90e130-e478-428b-ab06-236da930649d","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bb90e130-e478-428b-ab06-236da930649d","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"bb90e130-e478-428b-ab06-236da930649d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"52313568-f68f-47a6-b7ed-34a430893350","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"52313568-f68f-47a6-b7ed-34a430893350","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"52313568-f68f-47a6-b7ed-34a430893350","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6b8292e1-f174-450d-8909-6b3f8d01d2c8","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6b8292e1-f174-450d-8909-6b3f8d01d2c8","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"6b8292e1-f174-450d-8909-6b3f8d01d2c8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"754ac22d-e1eb-41b4-8bfe-ec8719c961b2","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"754ac22d-e1eb-41b4-8bfe-ec8719c961b2","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"754ac22d-e1eb-41b4-8bfe-ec8719c961b2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"befb933c-5f18-4e8b-9782-eff2329c7711","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"befb933c-5f18-4e8b-9782-eff2329c7711","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"befb933c-5f18-4e8b-9782-eff2329c7711","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f51d7ccf-1135-4189-b334-515951ab142f","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f51d7ccf-1135-4189-b334-515951ab142f","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"f51d7ccf-1135-4189-b334-515951ab142f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3337dd2e-45e5-4e02-adb9-2e8e79819745","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3337dd2e-45e5-4e02-adb9-2e8e79819745","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"3337dd2e-45e5-4e02-adb9-2e8e79819745","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"757e68bb-48de-40dd-b8e2-a3663853c7b3","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"757e68bb-48de-40dd-b8e2-a3663853c7b3","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"757e68bb-48de-40dd-b8e2-a3663853c7b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f0c032fb-8ea1-4862-802c-2ac5cad3b552","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f0c032fb-8ea1-4862-802c-2ac5cad3b552","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"f0c032fb-8ea1-4862-802c-2ac5cad3b552","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3635d22a-9d66-421b-b7b4-458182de24bc","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3635d22a-9d66-421b-b7b4-458182de24bc","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"3635d22a-9d66-421b-b7b4-458182de24bc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8f760229-6147-493d-b3d6-06d8428f875a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8f760229-6147-493d-b3d6-06d8428f875a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"8f760229-6147-493d-b3d6-06d8428f875a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9f2502d7-193e-44b2-9a27-ff73477518c7","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9f2502d7-193e-44b2-9a27-ff73477518c7","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"9f2502d7-193e-44b2-9a27-ff73477518c7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ffd7744a-0c67-42b8-b427-d1540250bce8","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ffd7744a-0c67-42b8-b427-d1540250bce8","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"ffd7744a-0c67-42b8-b427-d1540250bce8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c5c6e622-f421-4944-a523-a0bd19d52542","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c5c6e622-f421-4944-a523-a0bd19d52542","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"c5c6e622-f421-4944-a523-a0bd19d52542","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0b970e6c-4694-4b93-aa7e-57090417ad35","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0b970e6c-4694-4b93-aa7e-57090417ad35","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"0b970e6c-4694-4b93-aa7e-57090417ad35","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0c775a8d-b5ae-431d-9495-99f6aac98f24","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0c775a8d-b5ae-431d-9495-99f6aac98f24","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"0c775a8d-b5ae-431d-9495-99f6aac98f24","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e525740d-e065-475e-9df2-94988d7ab56b","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e525740d-e065-475e-9df2-94988d7ab56b","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"e525740d-e065-475e-9df2-94988d7ab56b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a54a27cb-8453-4679-8502-b65b621fd3f6","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a54a27cb-8453-4679-8502-b65b621fd3f6","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"a54a27cb-8453-4679-8502-b65b621fd3f6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"35cb12d0-6e67-464a-8f26-90873231c9ec","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"35cb12d0-6e67-464a-8f26-90873231c9ec","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"35cb12d0-6e67-464a-8f26-90873231c9ec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d5ce888a-8314-4e3e-ba93-8bb1084bce0e","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d5ce888a-8314-4e3e-ba93-8bb1084bce0e","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"d5ce888a-8314-4e3e-ba93-8bb1084bce0e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89620598-f2ee-45da-8f53-1dbc162eb9a9","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89620598-f2ee-45da-8f53-1dbc162eb9a9","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"89620598-f2ee-45da-8f53-1dbc162eb9a9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"938dc615-72ca-46dd-9df3-f1b31631da49","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"938dc615-72ca-46dd-9df3-f1b31631da49","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"938dc615-72ca-46dd-9df3-f1b31631da49","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb41ba08-5354-488d-8d40-b4f82461597e","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb41ba08-5354-488d-8d40-b4f82461597e","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"eb41ba08-5354-488d-8d40-b4f82461597e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88bf910d-3e50-4ee4-98eb-5a3bd4a54efd","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88bf910d-3e50-4ee4-98eb-5a3bd4a54efd","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"88bf910d-3e50-4ee4-98eb-5a3bd4a54efd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"86f03ee4-1f75-4a83-a277-c13cde09f205","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"86f03ee4-1f75-4a83-a277-c13cde09f205","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"86f03ee4-1f75-4a83-a277-c13cde09f205","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1124a320-e33c-4826-8424-247052cc49ce","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1124a320-e33c-4826-8424-247052cc49ce","path":"sprites/SprArrowBase/SprArrowBase.yy",},"LayerId":{"name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","name":"1124a320-e33c-4826-8424-247052cc49ce","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 27.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"344eaa7a-aaba-407e-be7d-81035f74810c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7315ad1f-c321-4fea-b36d-72515594faf1","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3a7739e5-4734-4ea7-bcef-2241afb4bca2","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bb90e130-e478-428b-ab06-236da930649d","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b3efc4bf-aca0-40f2-b679-8e131003a5e0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"52313568-f68f-47a6-b7ed-34a430893350","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3ffe064-eb4c-4094-a8fd-08ac3a2987b2","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6b8292e1-f174-450d-8909-6b3f8d01d2c8","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c7bc4ae7-ddda-416f-99d4-6c35da9e3a3d","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"754ac22d-e1eb-41b4-8bfe-ec8719c961b2","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3f51b0fd-71b1-4054-8a1f-72b0d399c981","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"befb933c-5f18-4e8b-9782-eff2329c7711","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2c7d0573-a33e-4430-a751-83618f9be3c2","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f51d7ccf-1135-4189-b334-515951ab142f","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a0d5ce1-30fb-48fe-9ebe-408732ddd34b","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3337dd2e-45e5-4e02-adb9-2e8e79819745","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1300988-fee0-46da-9d4f-009a154acb4f","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"757e68bb-48de-40dd-b8e2-a3663853c7b3","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"753ea4ee-8133-4d1b-9011-731b2b711c64","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0c032fb-8ea1-4862-802c-2ac5cad3b552","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"18db13a9-7cfd-4d89-9bda-8fba343c41c6","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3635d22a-9d66-421b-b7b4-458182de24bc","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2cb011f8-12c9-4efc-9dec-09e3f235a38b","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8f760229-6147-493d-b3d6-06d8428f875a","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b45f72e9-5322-46ff-84ad-d1c06191a121","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9f2502d7-193e-44b2-9a27-ff73477518c7","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b3709c2e-f873-437c-b12f-01917081ab54","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ffd7744a-0c67-42b8-b427-d1540250bce8","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"635658b0-0a90-4d83-99ed-0a345ae84376","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c5c6e622-f421-4944-a523-a0bd19d52542","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7ed92860-e9c4-472a-bb7d-14c03955e571","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0b970e6c-4694-4b93-aa7e-57090417ad35","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82e0f19e-a2fa-4940-80d7-49ec64c4bf17","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0c775a8d-b5ae-431d-9495-99f6aac98f24","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"524e479a-25d2-45e8-8a54-8e154607e3e3","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e525740d-e065-475e-9df2-94988d7ab56b","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d8cbf130-af4b-4cf0-b1c1-c676b1184606","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a54a27cb-8453-4679-8502-b65b621fd3f6","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c116cc9a-1556-46ec-a7e0-fc41d70f3740","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"35cb12d0-6e67-464a-8f26-90873231c9ec","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4a0d1f82-ae61-4a93-97ec-002ca331173e","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d5ce888a-8314-4e3e-ba93-8bb1084bce0e","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"24371c20-edff-4975-b5d1-eebd4d3a19e3","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89620598-f2ee-45da-8f53-1dbc162eb9a9","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"391dabe0-53a5-4e25-81b5-26af711118f2","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"938dc615-72ca-46dd-9df3-f1b31631da49","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"66e90e0e-ed61-4184-a1ca-afa35422022d","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb41ba08-5354-488d-8d40-b4f82461597e","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"30467c04-fede-4382-96ee-bc2778abc6a5","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88bf910d-3e50-4ee4-98eb-5a3bd4a54efd","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7ad7f767-8db5-4294-bf81-890a1b5051f3","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"86f03ee4-1f75-4a83-a277-c13cde09f205","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3583ce1-bd13-405c-bd24-ae5a7ddac27e","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1124a320-e33c-4826-8424-247052cc49ce","path":"sprites/SprArrowBase/SprArrowBase.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SprArrowBase","path":"sprites/SprArrowBase/SprArrowBase.yy",},
    "resourceVersion": "1.3",
    "name": "SprArrowBase",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5454f537-9b96-42c0-bb4c-35ed3e2e376a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "SprArrowBase",
  "tags": [],
  "resourceType": "GMSprite",
}