{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 149,
  "bbox_top": 0,
  "bbox_bottom": 91,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 150,
  "height": 92,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 100,
  "gridY": 100,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a1ed342b-38ea-4e62-8aca-eac845ccd073","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a1ed342b-38ea-4e62-8aca-eac845ccd073","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"a1ed342b-38ea-4e62-8aca-eac845ccd073","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"780920f8-b264-43e6-8635-b4a646d05a03","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"780920f8-b264-43e6-8635-b4a646d05a03","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"780920f8-b264-43e6-8635-b4a646d05a03","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e182e1ad-1825-492c-b8b7-d86dc4812733","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e182e1ad-1825-492c-b8b7-d86dc4812733","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"e182e1ad-1825-492c-b8b7-d86dc4812733","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"589a9507-245d-482e-8529-1001a043ed0f","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"589a9507-245d-482e-8529-1001a043ed0f","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"589a9507-245d-482e-8529-1001a043ed0f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"308aec88-4068-424b-8dc1-b52bbb0d7d4c","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"308aec88-4068-424b-8dc1-b52bbb0d7d4c","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"308aec88-4068-424b-8dc1-b52bbb0d7d4c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"90994b69-692d-4422-8f68-e7455dc0209d","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90994b69-692d-4422-8f68-e7455dc0209d","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"90994b69-692d-4422-8f68-e7455dc0209d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d40007e2-43dc-4328-8872-8a97276daf4b","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d40007e2-43dc-4328-8872-8a97276daf4b","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"d40007e2-43dc-4328-8872-8a97276daf4b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a06ef4d8-c493-4261-a916-be20e426e2e4","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a06ef4d8-c493-4261-a916-be20e426e2e4","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"a06ef4d8-c493-4261-a916-be20e426e2e4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f12254ec-aff7-48db-b71f-ec69ce182c83","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f12254ec-aff7-48db-b71f-ec69ce182c83","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"f12254ec-aff7-48db-b71f-ec69ce182c83","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e4764673-90b3-46fd-a927-eb1144ff198b","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e4764673-90b3-46fd-a927-eb1144ff198b","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"e4764673-90b3-46fd-a927-eb1144ff198b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2758f459-c5df-4853-85b8-53a20d916fbf","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2758f459-c5df-4853-85b8-53a20d916fbf","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"2758f459-c5df-4853-85b8-53a20d916fbf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70d79ccf-9407-4a36-9f3e-8530f307d125","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70d79ccf-9407-4a36-9f3e-8530f307d125","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"70d79ccf-9407-4a36-9f3e-8530f307d125","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1ec7f2f3-f1c0-49ac-94b5-ad174ebf8735","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1ec7f2f3-f1c0-49ac-94b5-ad174ebf8735","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"1ec7f2f3-f1c0-49ac-94b5-ad174ebf8735","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"78c9106a-6683-4664-a7b2-38ce6814e1ef","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78c9106a-6683-4664-a7b2-38ce6814e1ef","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"78c9106a-6683-4664-a7b2-38ce6814e1ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f96c18a2-2d49-4ca7-bcd4-a6e74fbaed9a","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f96c18a2-2d49-4ca7-bcd4-a6e74fbaed9a","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"f96c18a2-2d49-4ca7-bcd4-a6e74fbaed9a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a9643226-048d-4a8a-bca6-af19f24b02cf","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9643226-048d-4a8a-bca6-af19f24b02cf","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"a9643226-048d-4a8a-bca6-af19f24b02cf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"520715b3-5efd-4004-b971-0754899bb9b8","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"520715b3-5efd-4004-b971-0754899bb9b8","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"520715b3-5efd-4004-b971-0754899bb9b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2358e179-d565-4d64-9234-2adf2ae1b786","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2358e179-d565-4d64-9234-2adf2ae1b786","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"2358e179-d565-4d64-9234-2adf2ae1b786","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"80065a3a-5732-4aed-9965-7f4f134339d1","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"80065a3a-5732-4aed-9965-7f4f134339d1","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"80065a3a-5732-4aed-9965-7f4f134339d1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"82548195-d090-46c6-ba1a-783ad1eb72a1","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"82548195-d090-46c6-ba1a-783ad1eb72a1","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"82548195-d090-46c6-ba1a-783ad1eb72a1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9327684-a6a9-4eda-bba2-9dc08044592f","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9327684-a6a9-4eda-bba2-9dc08044592f","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"d9327684-a6a9-4eda-bba2-9dc08044592f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b2a08783-db1c-4671-8e67-fcc85d3b26fd","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b2a08783-db1c-4671-8e67-fcc85d3b26fd","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"b2a08783-db1c-4671-8e67-fcc85d3b26fd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0dcb3a13-2e73-4d31-b4b6-826cd2e42a09","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0dcb3a13-2e73-4d31-b4b6-826cd2e42a09","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"0dcb3a13-2e73-4d31-b4b6-826cd2e42a09","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3d1c9bb1-f81a-46e3-b800-2e80e3ecbb29","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3d1c9bb1-f81a-46e3-b800-2e80e3ecbb29","path":"sprites/SprGrass2/SprGrass2.yy",},"LayerId":{"name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","name":"3d1c9bb1-f81a-46e3-b800-2e80e3ecbb29","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 24.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9b083561-09a7-4d3d-935e-05019a094c1c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a1ed342b-38ea-4e62-8aca-eac845ccd073","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9836c8c7-b91a-4f15-970f-26afa7f5988a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"780920f8-b264-43e6-8635-b4a646d05a03","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6547c16c-e2b8-48a0-8ebd-8d9b392db357","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e182e1ad-1825-492c-b8b7-d86dc4812733","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2742e19e-5a07-4fda-980e-88e6bca3ea05","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"589a9507-245d-482e-8529-1001a043ed0f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"abe36343-9f01-4455-97db-f127e1fe79ac","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"308aec88-4068-424b-8dc1-b52bbb0d7d4c","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dbd7c856-26c8-4ed9-9156-b80b32e099c7","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90994b69-692d-4422-8f68-e7455dc0209d","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3722cbd7-f2c1-45b7-9735-528414497266","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d40007e2-43dc-4328-8872-8a97276daf4b","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"12bae6f0-dd11-408b-9eab-ed090c94df62","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a06ef4d8-c493-4261-a916-be20e426e2e4","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6c364c8f-5a18-45a2-bd7d-a0624dd19574","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f12254ec-aff7-48db-b71f-ec69ce182c83","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf0254ea-90ed-48e8-8e58-2b8c4fed6008","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e4764673-90b3-46fd-a927-eb1144ff198b","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bc1baf11-d02e-4c7a-ad74-4dbb769648fc","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2758f459-c5df-4853-85b8-53a20d916fbf","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"41086ead-ecb1-4db7-97d9-60286daf5474","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70d79ccf-9407-4a36-9f3e-8530f307d125","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9f2563b3-0968-44f5-bf3e-875582ae494d","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1ec7f2f3-f1c0-49ac-94b5-ad174ebf8735","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"77bf758d-9f03-4408-a720-5566f3f108a6","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78c9106a-6683-4664-a7b2-38ce6814e1ef","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"58b68b6c-d0f3-4b24-a5f4-74382f7a06ff","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f96c18a2-2d49-4ca7-bcd4-a6e74fbaed9a","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f62f455d-927b-48dd-9a12-53f6b7ea0f6c","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9643226-048d-4a8a-bca6-af19f24b02cf","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00eec863-d3fb-4995-8822-6d9fb448dd03","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"520715b3-5efd-4004-b971-0754899bb9b8","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"05252e0f-6cc8-4269-9f08-4ac32b75518b","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2358e179-d565-4d64-9234-2adf2ae1b786","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf8a738e-7191-4828-a695-9184751f8ceb","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"80065a3a-5732-4aed-9965-7f4f134339d1","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e5fbf9aa-c050-4d0b-9578-fabadf2a0d33","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"82548195-d090-46c6-ba1a-783ad1eb72a1","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"abe7f4df-aca7-4cd4-8982-5bc194e592ff","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9327684-a6a9-4eda-bba2-9dc08044592f","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc3a2c78-2aad-4ac6-af41-36e9e9463dec","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b2a08783-db1c-4671-8e67-fcc85d3b26fd","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"601fd8e7-dd69-4bd5-adad-668152061474","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0dcb3a13-2e73-4d31-b4b6-826cd2e42a09","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d869a965-de36-4ba7-abfb-3947bd0ca4f3","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3d1c9bb1-f81a-46e3-b800-2e80e3ecbb29","path":"sprites/SprGrass2/SprGrass2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 65,
    "yorigin": 71,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SprGrass2","path":"sprites/SprGrass2/SprGrass2.yy",},
    "resourceVersion": "1.3",
    "name": "SprGrass2",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4796bb5d-1a02-4ce3-a113-9c5b9831432f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "SprGrass2",
  "tags": [],
  "resourceType": "GMSprite",
}