<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="ash" tilewidth="128" tileheight="192" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="128" height="192" source="sprites/blank_tile_128.png"/>
 </tile>
 <tile id="1">
  <image width="128" height="192" source="sprites/forest_128.png"/>
 </tile>
 <tile id="2">
  <image width="128" height="192" source="sprites/water_128.png"/>
 </tile>
 <tile id="3">
  <image width="128" height="192" source="sprites/mountian_128.png"/>
 </tile>
</tileset>
